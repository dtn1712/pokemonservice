package com.rockitgaming.pokemon.service;

import com.rockitgaming.pokemon.data.model.Constants;
import com.rockitgaming.pokemon.data.model.entity.account.Account;
import com.rockitgaming.pokemon.data.model.entity.account.AccountAttemptLogin;
import com.rockitgaming.pokemon.data.model.entity.account.AccountLock;
import com.rockitgaming.pokemon.data.model.etc.Keyword;
import com.rockitgaming.pokemon.data.model.repository.account.AccountAttemptLoginRepository;
import com.rockitgaming.pokemon.data.model.repository.account.AccountLockRepository;
import com.rockitgaming.pokemon.data.model.repository.account.AccountRepository;
import com.rockitgaming.pokemon.data.model.util.AccountUtils;
import com.rockitgaming.pokemon.service.exceptions.*;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Objects;

@Service
public class AccountService extends BaseService {

    private static final Logger logger = LogManager.getLogger(AccountService.class);

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private AccountRepository accountRepository;

    @Autowired
    private AccountAttemptLoginRepository accountAttemptLoginRepository;

    @Autowired
    private AccountLockRepository accountLockRepository;

    public Account login(String loginType, String loginValue, String password, String loginIP) {
        if (loginType == null || loginValue == null || password == null || loginIP == null) {
            throw new InvalidInputException("Login type, login value, password and loginIP cannot be null");
        }

        // There should be only one single account lock object for current timestamp
        AccountLock accountLock = getCurrentAccountLock(loginType, loginValue);
        if (isAccountLocked(accountLock, loginIP)) {
            throw new AccountAccessException(String.format("This IP %s is LOCKED to access this account. " +
                    "This account is LOCKED from %s to %s", loginIP, accountLock.getLockStartTime(), accountLock.getLockEndTime()));
        } else {
            try {
                Account account = getAccountByCredential(loginType, loginValue, password);
                if (account == null) {
                    markFailedLoginAttempt(loginType, loginValue, loginIP);
                    throw new AccountAccessException("Invalid account credentials");
                } else {
                    markSuccessLoginAttempt(account, loginIP);
                    return AccountUtils.getSecureAccount(account);
                }
            } catch (InvalidInputException e) {
                logger.error("Failed to retrieve the account object. Input is invalid",e);
                throw new AccountAccessException("Failed to retrieve the account object. Input is invalid",e);
            }
        }
    }

    @Transactional
    public Account register(Account account, String loginIP) {
        if (account == null || account.getUsername() == null || account.getEmail() == null || account.getPassword() == null) {
            throw new InvalidInputException("Username and email and password cannot be null");
        }

        // Check if account exist with username and email. If not, create. If yes, not allow to create
        boolean isExist = isAccountExist(account.getUsername(), account.getEmail());
        if (!isExist) {
            try {
                account.setPassword(passwordEncoder.encode(account.getPassword()));
                Account result = accountRepository.saveAndFlush(account);
                markSuccessLoginAttempt(result, loginIP);
                return AccountUtils.getSecureAccount(result);
            } catch (Exception e) {
                logger.error("Failed to register account",e);
                throw new AccountRegisterException("Failed to register account. Please try again",e);
            }
        } else {
            throw new AccountRegisterException("This username is not available");
        }
    }

    @Transactional
    public void logout(Account account) {
        if (account == null || account.getUsername() == null) {
            throw new InvalidInputException("Username cannot be null");
        }

        AccountAttemptLogin accountAttemptLogin = accountAttemptLoginRepository.getLastAttemptLogin(account.getUsername());

        if (accountAttemptLogin == null || !accountAttemptLogin.isLoginSuccess()) {
            throw new InvalidInputException("This user haven't logged in. Cannot log out");
        }

        if (accountAttemptLogin.isSessionLogout()) {
            throw new InvalidRequestException("This user already logged out");
        }
        try {
            accountAttemptLogin.setSessionLogout(true);
            accountAttemptLoginRepository.saveAndFlush(accountAttemptLogin);
            deleteBulkCacheObject(generateCacheKey(Account.class.getName(), Account.CURRENT_LOGIN_PATTERN_CACHE_KEY + "*"));
        } catch (Exception e) {
            logger.error("Failed to logout account",e);
            throw new UpdateDataException("Failed to logout account. Please try again",e);
        }
    }

    @Transactional(readOnly = true)
    public boolean isCurrentLogin(String username, String clientIP) {
        String cacheKey = generateCacheKey(Account.class.getName(),
                Account.CURRENT_LOGIN_PATTERN_CACHE_KEY + Constants.CACHE_KEY_SEPARATOR + Account.IS_CURRENT_LOGIN_CACHE_KEY,
                username + Constants.CACHE_KEY_SEPARATOR + clientIP);
        Boolean cacheData = (Boolean) getCacheObject(cacheKey, Boolean.class);
        if (cacheData != null) {
            return cacheData;
        } else {
            boolean isCurrentLogin = accountRepository.isCurrentLogin(username, clientIP);
            setCacheObject(cacheKey, isCurrentLogin);
            return isCurrentLogin;
        }
    }

    @Transactional(readOnly = true)
    public Account getCurrentLoginAccount(String username, String loginIP) {
        String cacheKey = generateCacheKey(Account.class.getName(),
                Account.CURRENT_LOGIN_PATTERN_CACHE_KEY + Constants.CACHE_KEY_SEPARATOR + Account.GET_CURRENT_LOGIN_ACCOUNT_CACHE_KEY,
                username + Constants.CACHE_KEY_SEPARATOR + loginIP);
        Account cacheData = (Account) getCacheObject(cacheKey, Account.class);
        if (cacheData != null) {
            return cacheData;
        } else {
            Account currentLoginAccount = AccountUtils.getSecureAccount(accountRepository.getCurrentLoginAccount(username, loginIP));
            setCacheObject(cacheKey, currentLoginAccount);
            return currentLoginAccount;
        }
    }

    @Transactional(readOnly = true)
    public AccountLock getCurrentAccountLock(String loginType, String loginValue) {
        if (Objects.equals("username", loginType)) {
            return accountLockRepository.getCurrentAccountLockByUsername(loginValue, new DateTime().toString(), new DateTime().toString());
        } else if (Objects.equals("email", loginType)) {
            return accountLockRepository.getCurrentAccountLockByEmail(loginValue, new DateTime().toString(), new DateTime().toString());
        } else if (Objects.equals("phone", loginType)) {
            return accountLockRepository.getCurrentAccountLockByPhone(loginValue, new DateTime().toString(), new DateTime().toString());
        }
        return null;
    }

    public boolean isAccountLocked(AccountLock accountLock, String loginIP) {
        if (accountLock == null) return false;
        if (StringUtils.isBlank(accountLock.getLockIP())) return false;
        if (accountLock.getLockIP().toUpperCase().equals(Keyword.ALL.toString())) return true;
        if (accountLock.getLockIP().contains(loginIP)) return true;
        return false;
    }

    public boolean checkAccountLockedByUsername(String username, String loginIP) {
        AccountLock accountLock = getCurrentAccountLock("username", username);
        return isAccountLocked(accountLock, loginIP);
    }

    @Transactional(readOnly = true)
    public boolean isAccountExist(String username, String email) {
        Long accountCount = accountRepository.countByUsernameAndEmail(username, email);
        return (accountCount == 0) ? false : true;
    }

    @Transactional
    private void markFailedLoginAttempt(String loginType, String loginValue, String loginIP) {
        Long accountId = getAccountId(loginType, loginValue);

        if (accountId == null) {
            throw new AccountAccessException(String.format("For login type %s, there is no account with value %s", loginType, loginValue));
        }

        Account account = new Account();
        account.setId(accountId);

        DateTime end = new DateTime();
        DateTime start = new DateTime().minusMinutes(Constants.MINUTES_DURATION_FOR_MAX_FAILED_ATTEMPT_LOGIN);
        Integer failedCount = countFailedAttemptInPeriod(loginType, loginValue, loginIP, start, end);
        if (failedCount == null) {
            throw new AccountAccessException(String.format("Login type %s is not valid", loginType));
        }
        if (failedCount >= Constants.MAX_FAILED_ATTEMPT_LOGIN) {
            // Lock it
            AccountLock accountLock = new AccountLock();
            accountLock.setAccount(account);
            accountLock.setLockIP(loginIP);
            accountLock.setReason("Failed login too many time in the allowed period");
            accountLock.setDuration(Constants.FAILED_LOGIN_ACCOUNT_LOCK_HOUR_DURATION);
            accountLock.setLockStartTime(new DateTime());
            accountLock.setLockEndTime(new DateTime().plusHours(Constants.FAILED_LOGIN_ACCOUNT_LOCK_HOUR_DURATION));
            accountLockRepository.saveAndFlush(accountLock);
        } else {
            // Create one more failed attempt
            AccountAttemptLogin accountAttemptLogin = new AccountAttemptLogin();
            accountAttemptLogin.setAccount(account);
            accountAttemptLogin.setLoginIP(loginIP);
            accountAttemptLogin.setLoginSuccess(false);
            accountAttemptLoginRepository.saveAndFlush(accountAttemptLogin);
        }
    }

    @Transactional
    private void markSuccessLoginAttempt(Account account, String loginIP) {
        account.setLastSuccessLoginTime(new DateTime());
        accountRepository.saveAndFlush(account);

        AccountAttemptLogin accountAttemptLogin = new AccountAttemptLogin();
        accountAttemptLogin.setAccount(account);
        accountAttemptLogin.setLoginIP(loginIP);
        accountAttemptLogin.setLoginSuccess(true);
        accountAttemptLogin.setLoginTime(account.getLastSuccessLoginTime());
        accountAttemptLoginRepository.saveAndFlush(accountAttemptLogin);
    }


    private Account getAccountByCredential(String loginType, String loginValue, String password) throws InvalidInputException {
        Account account = findAccount(loginType, loginValue);
        if (account != null && passwordEncoder.matches(password, account.getPassword())) {
            return account;
        }
        return null;
    }

    @Transactional(readOnly = true)
    private Account findAccount(String findType, String findValue) {
        if (Objects.equals("username", findType)) {
            return accountRepository.findByUsername(findValue);
        } else if (Objects.equals("email", findType)) {
            return accountRepository.findByEmail(findValue);
        } else if (Objects.equals("phone", findType)) {
            return accountRepository.findByPhone(findValue);
        }
        return null;
    }

    @Transactional(readOnly = true)
    private Long getAccountId(String loginType, String loginValue) {
        if (Objects.equals("username", loginType)) {
            return accountRepository.getAccountIdByUsername(loginValue);
        } else if (Objects.equals("email", loginType)) {
            return accountRepository.getAccountIdByEmail(loginValue);
        } else if (Objects.equals("phone", loginType)) {
            return accountRepository.getAccountIdByPhone(loginValue);
        }
        return null;
    }

    @Transactional(readOnly = true)
    private Integer countFailedAttemptInPeriod(String loginType, String loginValue, String loginIP, DateTime start, DateTime end) {
        if (Objects.equals("username", loginType)) {
            return accountAttemptLoginRepository.countFailedAttemptByUsernameInPeriod(loginValue, loginIP, start.toString(), end.toString());
        } else if (Objects.equals("email", loginType)) {
            return accountAttemptLoginRepository.countFailedAttemptByEmailInPeriod(loginValue, loginIP, start.toString(), end.toString());
        } else if (Objects.equals("phone", loginType)) {
            return accountAttemptLoginRepository.countFailedAttemptByPhoneInPeriod(loginValue, loginIP, start.toString(), end.toString());
        }
        return null;
    }

}
