package com.rockitgaming.pokemon.service;

import com.rockitgaming.pokemon.data.model.entity.player.Friend;
import com.rockitgaming.pokemon.data.model.entity.player.FriendRequest;
import com.rockitgaming.pokemon.data.model.entity.player.Player;
import com.rockitgaming.pokemon.data.model.repository.player.FriendRepository;
import com.rockitgaming.pokemon.data.model.repository.player.FriendRequestRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
public class FriendService extends BaseService {

    @Autowired
    private FriendRepository friendRepository;

    @Autowired
    private FriendRequestRepository friendRequestRepository;


    @Transactional(readOnly = true)
    public List<Player> getPlayerFriends(Player player) {
        String cacheKey = generateCacheKey("friend", "getPlayerFriends", player.getName());
        List<Player> cacheData = (List<Player>) getCacheObject(cacheKey, List.class);
        if (cacheData != null) {
            return cacheData;
        } else {
            List<Player> friends = friendRepository.getPlayerFriends(Long.toString(player.getId()));
            setCacheObject(cacheKey, friends);
            return friends;
        }
    }

    @Transactional
    public Friend createFriend(Player player1, Player player2) {
        Friend friend = new Friend(player1, player2);
        Friend createdFriend = friendRepository.saveAndFlush(friend);
        clearFriendCache(player1.getId(), player2.getId());
        return createdFriend;
    }

    @Transactional
    public void deleteFriend(Friend friend) {
        friendRepository.delete(friend);
        clearFriendCache(friend.getPlayer1().getId(), friend.getPlayer2().getId());
    }

    private void clearFriendCache(Long player1Id, Long player2Id) {
        deleteCacheObject(generateCacheKey("friend", "getPlayerFriends", Long.toString(player1Id)));
        deleteCacheObject(generateCacheKey("friend", "getPlayerFriends", Long.toString(player2Id)));
    }

    @Transactional
    public FriendRequest createFriendRequest(Player fromPlayer, Player toPlayer) {
        FriendRequest friendRequest = new FriendRequest(fromPlayer, toPlayer);
        FriendRequest createdFriendRequest = friendRequestRepository.saveAndFlush(friendRequest);
        addFriendRequestCache(createdFriendRequest);
        return createdFriendRequest;
    }

    @Transactional
    public void updateFriendRequest(FriendRequest friendRequest) {
        friendRequestRepository.saveAndFlush(friendRequest);
        clearFriendRequestCache(friendRequest);
    }

    @Transactional(readOnly = true)
    public List<FriendRequest> findFriendRequestByFromPlayer(Player fromPlayer) {
        String cacheKey = generateCacheKey("friendRequest", "findByFromPlayer", fromPlayer.getName());
        List<FriendRequest> cacheData = (List<FriendRequest>) getCacheObject(cacheKey, List.class);
        if (cacheData != null) {
            return cacheData;
        } else {
            List<FriendRequest> friendRequests = friendRequestRepository.findByFromPlayer(fromPlayer);
            setCacheObject(cacheKey, friendRequests);
            return friendRequests;
        }
    }

    @Transactional(readOnly = true)
    public List<FriendRequest> findFriendRequestByToPlayer(Player toPlayer) {
        String cacheKey = generateCacheKey("friendRequest", "findByToPlayer", toPlayer.getName());
        List<FriendRequest> cacheData = (List<FriendRequest>) getCacheObject(cacheKey, List.class);
        if (cacheData != null) {
            return cacheData;
        } else {
            List<FriendRequest> friendRequests = friendRequestRepository.findByToPlayer(toPlayer);
            setCacheObject(cacheKey, friendRequests);
            return friendRequests;
        }
    }

    private void clearFriendRequestCache(FriendRequest friendRequest) {
        deleteCacheObject(generateCacheKey("friendRequest", "findFriendRequestByFromPlayer", friendRequest.getFromPlayer().getName()));
        deleteCacheObject(generateCacheKey("friendRequest", "findFriendRequestByToPlayer", friendRequest.getToPlayer().getName()));
    }

    private void addFriendRequestCache(FriendRequest friendRequest){
        addFriendRequestCacheByKey(generateCacheKey("friendRequest", "findFriendRequestByFromPlayer", friendRequest.getFromPlayer().getName()), friendRequest);
        addFriendRequestCacheByKey(generateCacheKey("friendRequest", "findFriendRequestByToPlayer", friendRequest.getToPlayer().getName()), friendRequest);
    }

    private void addFriendRequestCacheByKey(String cacheKey, FriendRequest friendRequest) {
        List<FriendRequest> cacheData = (List<FriendRequest>) getCacheObject(cacheKey, List.class);
        if (cacheData == null) {
            cacheData = new ArrayList<>();
        }
        cacheData.add(friendRequest);
        setCacheObject(cacheKey, cacheData);
    }
}
