package com.rockitgaming.pokemon.service;

import com.rockitgaming.pokemon.data.model.entity.item.Item;
import com.rockitgaming.pokemon.data.model.entity.item.ItemCategory;
import com.rockitgaming.pokemon.data.model.entity.item.ItemPocket;
import com.rockitgaming.pokemon.data.model.repository.item.ItemCategoryRepository;
import com.rockitgaming.pokemon.data.model.repository.item.ItemPocketRepository;
import com.rockitgaming.pokemon.data.model.repository.item.ItemRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class ItemService extends BaseService {

    private static final Logger logger = LogManager.getLogger(ItemService.class);

    @Autowired
    private ItemRepository itemRepository;

    @Autowired
    private ItemCategoryRepository itemCategoryRepository;

    @Autowired
    private ItemPocketRepository itemPocketRepository;

    @Transactional(readOnly = true)
    public List<ItemCategory> findAllItemCategories() {
        String cacheKey =  generateCacheKey("itemCategory", "findAll");
        List<ItemCategory> cacheData = (List<ItemCategory>) getCacheObject(cacheKey, List.class);
        if (cacheData != null) {
            return cacheData;
        } else {
            List<ItemCategory> itemCategories = itemCategoryRepository.findAll();
            setCacheObject(cacheKey, itemCategories);
            return itemCategories;
        }
    }

    @Transactional(readOnly = true)
    public ItemCategory findItemCategoryByKey(String key) {
        String cacheKey = generateCacheKey("itemCategory", "findByKey", key);
        ItemCategory cacheData = (ItemCategory) getCacheObject(cacheKey, ItemCategory.class);
        if (cacheData != null) {
            return cacheData;
        } else {
            ItemCategory itemCategory = itemCategoryRepository.findByKey(key);
            setCacheObject(cacheKey, itemCategory);
            return itemCategory;
        }
    }

    @Transactional(readOnly = true)
    public List<ItemCategory> findItemCategoriesByPocket(ItemPocket pocket) {
        String cacheKey = generateCacheKey("itemCategory", "findByPocket", pocket.getKey());
        List<ItemCategory> cacheData = (List<ItemCategory>) getCacheObject(cacheKey, List.class);
        if (cacheData != null) {
            return cacheData;
        } else {
            List<ItemCategory> itemCategories = itemCategoryRepository.findByPocket(pocket);
            setCacheObject(cacheKey, itemCategories);
            return itemCategories;
        }
    }

    @Transactional(readOnly = true)
    public List<Item> findAllItems() {
        String cacheKey = generateCacheKey("item", "findAll");
        List<Item> cacheData = (List<Item>) getCacheObject(cacheKey, List.class);
        if (cacheData != null) {
            return cacheData;
        } else {
            List<Item> items = itemRepository.findAll();
            setCacheObject(cacheKey, items);
            return items;
        }
    }

    @Transactional(readOnly = true)
    public Item findItemByName(String name) {
        String cacheKey = generateCacheKey("item", "findByName", name);
        Item cacheData = (Item) getCacheObject(cacheKey, Item.class);
        if (cacheData != null) {
            return cacheData;
        } else {
            Item item = itemRepository.findByName(name);
            setCacheObject(cacheKey, item);
            return item;
        }
    }

    @Transactional(readOnly = true)
    public List<Item> findItemsByCategory(ItemCategory category) {
        String cacheKey = generateCacheKey("item", "findByCategory", category.getName());
        List<Item> cacheData = (List<Item>) getCacheObject(cacheKey, List.class);
        if (cacheData != null) {
            return cacheData;
        } else {
            List<Item> items = itemRepository.findByCategory(category);
            setCacheObject(cacheKey, items);
            return items;
        }
    }

    @Transactional(readOnly = true)
    public List<Item> findItemsByCostGreaterThan(Long cost) {
        String cacheKey = generateCacheKey("item", "findByCostGreaterThan", Long.toString(cost));
        List<Item> cacheData = (List<Item>) getCacheObject(cacheKey, List.class);
        if (cacheData != null) {
            return cacheData;
        } else {
            List<Item> items = itemRepository.findByCostGreaterThan(cost);
            setCacheObject(cacheKey, items);
            return items;
        }
    }

    @Transactional(readOnly = true)
    public List<Item> findItemsByCostGreaterThanEqual(Long cost) {
        String cacheKey = generateCacheKey("item", "findByCostGreaterThanEqual", Long.toString(cost));
        List<Item> cacheData = (List<Item>) getCacheObject(cacheKey, List.class);
        if (cacheData != null) {
            return cacheData;
        } else {
            List<Item> items = itemRepository.findByCostGreaterThanEqual(cost);
            setCacheObject(cacheKey, items);
            return items;
        }
    }

    @Transactional(readOnly = true)
    public List<Item> findItemsByCostLessThan(Long cost) {
        String cacheKey = generateCacheKey("item", "findByCostLessThan", Long.toString(cost));
        List<Item> cacheData = (List<Item>) getCacheObject(cacheKey, List.class);
        if (cacheData != null) {
            return cacheData;
        } else {
            List<Item> items = itemRepository.findByCostLessThan(cost);
            setCacheObject(cacheKey, items);
            return items;
        }
    }

    @Transactional(readOnly = true)
    public List<Item> findItemsByCostLessThanEqual(Long cost) {
        String cacheKey = generateCacheKey("item", "findByCostLessThanEqual", Long.toString(cost));
        List<Item> cacheData = (List<Item>) getCacheObject(cacheKey, List.class);
        if (cacheData != null) {
            return cacheData;
        } else {
            List<Item> items = itemRepository.findByCostLessThanEqual(cost);
            setCacheObject(cacheKey, items);
            return items;
        }
    }

    @Transactional(readOnly = true)
    public List<ItemPocket> findAllItemPockets() {
        String cacheKey = generateCacheKey("itemPocket", "findAll");
        List<ItemPocket> cacheData = (List<ItemPocket>) getCacheObject(cacheKey, List.class);
        if (cacheData != null) {
            return cacheData;
        } else {
            List<ItemPocket> itemPockets = itemPocketRepository.findAll();
            setCacheObject(cacheKey, itemPockets);
            return itemPockets;
        }
    }

    @Transactional(readOnly = true)
    public ItemPocket findItemPocketByKey(String key) {
        String cacheKey = generateCacheKey("itemPocket", "findByKey", key);
        ItemPocket cacheData = (ItemPocket) getCacheObject(cacheKey, ItemPocket.class);
        if (cacheData != null) {
            return cacheData;
        } else {
            ItemPocket itemPocket = itemPocketRepository.findByKey(key);
            setCacheObject(cacheKey, itemPocket);
            return itemPocket;
        }
    }
}
