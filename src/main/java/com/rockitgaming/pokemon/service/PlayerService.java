package com.rockitgaming.pokemon.service;

import com.rockitgaming.pokemon.data.model.entity.account.Account;
import com.rockitgaming.pokemon.data.model.entity.player.Player;
import com.rockitgaming.pokemon.data.model.entity.player.PlayerBagItem;
import com.rockitgaming.pokemon.data.model.entity.player.PlayerParty;
import com.rockitgaming.pokemon.data.model.entity.player.PlayerPokedex;
import com.rockitgaming.pokemon.service.exceptions.AccountAccessException;
import com.rockitgaming.pokemon.service.exceptions.UpdateDataException;
import com.rockitgaming.pokemon.data.model.repository.account.AccountRepository;
import com.rockitgaming.pokemon.data.model.repository.player.PlayerBagItemRepository;
import com.rockitgaming.pokemon.data.model.repository.player.PlayerPartyRepository;
import com.rockitgaming.pokemon.data.model.repository.player.PlayerPokedexRepository;
import com.rockitgaming.pokemon.data.model.repository.player.PlayerRepository;
import com.rockitgaming.pokemon.data.model.util.AccountUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
public class PlayerService extends BaseService {

    @Autowired
    private AccountRepository accountRepository;

    @Autowired
    private PlayerRepository playerRepository;

    @Autowired
    private PlayerBagItemRepository playerBagItemRepository;

    @Autowired
    private PlayerPartyRepository playerPartyRepository;

    @Autowired
    private PlayerPokedexRepository playerPokedexRepository;

    @Transactional
    public Player createPlayer(Player player) {
        if (existsByName(player.getName())) {
            throw new UpdateDataException("Player name already exist");
        }

        Player createdPlayer = playerRepository.saveAndFlush(player);
        updateCachePlayer(createdPlayer);
        return createdPlayer;
    }

    @Transactional
    public void updatePlayer(Player player) {
        playerRepository.saveAndFlush(player);
        updateCachePlayer(player);
    }

    @Transactional(readOnly = true)
    public Player getPlayer(String name) {
        String cacheKey = generateCacheKey(Player.class.getName(), Player.GET_PLAYER_CACHE_KEY, name);
        Player cacheData = (Player) getCacheObject(cacheKey, Player.class);
        if (cacheData != null) {
            return cacheData;
        } else {
            Player player = playerRepository.findByName(name);
            player.setAccount(AccountUtils.getSecureAccount(player.getAccount()));
            setCacheObject(cacheKey, player);
            return player;
        }
    }

    @Transactional(readOnly = true)
    public List<Player> getPlayersByAccount(Account account) throws AccountAccessException{
        String cacheKey = generateCacheKey(Player.class.getName(), Player.GET_PLAYERS_BY_ACCOUNT_CACHE_KEY, account.getUsername());
        List<Player> cacheData = (List<Player>) getCacheObject(cacheKey, List.class);
        if (cacheData != null) {
            return cacheData;
        } else {
            List<Player> players = playerRepository.findByAccount(account);
            for (Player player : players) {
                player.setAccount(AccountUtils.getSecureAccount(player.getAccount()));
            }
            setCacheObject(cacheKey, players);
            return players;
        }
    }

    @Transactional
    public Player choosePlayer(Account account, String playerName) {
        String cacheKey = generateCacheKey(Player.class.getName(), Player.GET_CURRENT_PLAYER_CACHE_KEY, account.getUsername());
        Player cacheData = (Player) getCacheObject(cacheKey, Player.class);
        if (cacheData != null) {
            return cacheData;
        } else {
            Player player = playerRepository.findByName(playerName);
            player.setAccount(AccountUtils.getSecureAccount(player.getAccount()));
            setCacheObject(cacheKey, player);

            account.setCurrentPlayer(player);
            accountRepository.saveAndFlush(account);

            return player;
        }
    }

    public boolean existsByName(String name) {
        return playerRepository.existsByName(name);
    }

    private void updateCachePlayer(Player player) {
        player.setAccount(AccountUtils.getSecureAccount(player.getAccount()));

        setCacheObject(generateCacheKey(Player.class.getName(), Player.GET_PLAYER_CACHE_KEY, player.getName()), player);

        String getCurrentPlayerCacheKey = generateCacheKey(Player.class.getName(), Player.GET_CURRENT_PLAYER_CACHE_KEY, player.getAccount().getUsername());
        Player getCurrentPlayerCacheData = (Player) getCacheObject(getCurrentPlayerCacheKey, Player.class);
        if (getCurrentPlayerCacheData != null && getCurrentPlayerCacheData.getId().equals(player.getId())) {
            setCacheObject(getCurrentPlayerCacheKey, player);
        }

        String getPlayersByAccountCacheKey = generateCacheKey(Player.class.getName(), Player.GET_PLAYERS_BY_ACCOUNT_CACHE_KEY, player.getAccount().getUsername());
        List<Player> getPlayersByAccountCacheData = (List<Player>) getCacheObject(getPlayersByAccountCacheKey, List.class);
        if (getPlayersByAccountCacheData != null) {
            List<Player> newAccountPlayersData = new ArrayList<>();
            getPlayersByAccountCacheData.stream().filter(accountPlayer -> accountPlayer != null).forEach(accountPlayer -> {
                if (accountPlayer.getId().equals(player.getId())) {
                    newAccountPlayersData.add(player);
                } else {
                    newAccountPlayersData.add(accountPlayer);
                }
            });
            setCacheObject(getPlayersByAccountCacheKey, newAccountPlayersData);
        }
    }

    @Transactional(readOnly = true)
    public List<PlayerBagItem> getPlayerBagItems(Player player) {
        String cacheKey = generateCacheKey(PlayerBagItem.class.getName(),  PlayerBagItem.GET_PLAYER_BAG_ITEMS_CACHE_KEY, player.getName());
        List<PlayerBagItem> cacheData = (List<PlayerBagItem>) getCacheObject(cacheKey, List.class);
        if (cacheData != null) {
            return cacheData;
        } else {
            List<PlayerBagItem> playerBagItems = playerBagItemRepository.findByPlayer(player);
            setCacheObject(cacheKey, playerBagItems);
            return playerBagItems;
        }
    }

    @Transactional
    public PlayerBagItem createPlayerBagItem(PlayerBagItem playerBagItem) {
        PlayerBagItem createdPlayerBagItem = playerBagItemRepository.saveAndFlush(playerBagItem);
        String cacheKey = generateCacheKey(PlayerBagItem.class.getName(), PlayerBagItem.GET_PLAYER_BAG_ITEMS_CACHE_KEY, playerBagItem.getPlayer().getName());
        List<PlayerBagItem> cacheData = (List<PlayerBagItem>) getCacheObject(cacheKey, List.class);
        if (cacheData == null) {
            cacheData = new ArrayList<>();
        }
        cacheData.add(createdPlayerBagItem);
        setCacheObject(cacheKey, cacheData);
        return createdPlayerBagItem;
    }

    @Transactional
    public void updatePlayerBagItem(PlayerBagItem playerBagItem) {
        playerBagItemRepository.saveAndFlush(playerBagItem);
        deleteCacheObject(generateCacheKey(PlayerBagItem.class.getName(), PlayerBagItem.GET_PLAYER_BAG_ITEMS_CACHE_KEY, playerBagItem.getPlayer().getName()));
    }

    @Transactional
    public void deletePlayerBagItem(PlayerBagItem playerBagItem) {
        playerBagItemRepository.delete(playerBagItem);
        deleteCacheObject(generateCacheKey(PlayerBagItem.class.getName(), PlayerBagItem.GET_PLAYER_BAG_ITEMS_CACHE_KEY, playerBagItem.getPlayer().getName()));
    }

    @Transactional
    public PlayerParty createPlayerParty(PlayerParty playerParty) {
        PlayerParty createdPlayerParty = playerPartyRepository.saveAndFlush(playerParty);
        setCacheObject(generateCacheKey(PlayerParty.class.getName(), PlayerParty.GET_PLAYER_PARTY_CACHE_KEY, playerParty.getPlayer().getName()), createdPlayerParty);
        return createdPlayerParty;
    }

    @Transactional
    public void updatePlayerParty(PlayerParty playerParty) {
        PlayerParty updatedPlayerParty = playerPartyRepository.saveAndFlush(playerParty);
        setCacheObject(generateCacheKey(PlayerParty.class.getName(), PlayerParty.GET_PLAYER_PARTY_CACHE_KEY, playerParty.getPlayer().getName()), updatedPlayerParty);
    }

    @Transactional(readOnly = true)
    public PlayerParty getPlayerParty(Player player) {
        String cacheKey = generateCacheKey(PlayerParty.class.getName(), PlayerParty.GET_PLAYER_PARTY_CACHE_KEY, player.getName());
        PlayerParty cacheData = (PlayerParty) getCacheObject(cacheKey, PlayerParty.class);
        if (cacheData != null) {
            return cacheData;
        } else {
            PlayerParty playerParty = playerPartyRepository.findByPlayer(player);
            setCacheObject(cacheKey, playerParty);
            return playerParty;
        }
    }

    @Transactional
    public PlayerPokedex createPlayerPokedex(PlayerPokedex playerPokedex) {
        PlayerPokedex createdPlayerPokedex = playerPokedexRepository.saveAndFlush(playerPokedex);
        setCacheObject(generateCacheKey(PlayerPokedex.class.getName(), PlayerPokedex.GET_PLAYER_POKEDEX_CACHE_KEY, playerPokedex.getPlayer().getName()), createdPlayerPokedex);
        return createdPlayerPokedex;
    }

    @Transactional
    public void updatePlayerPokedex(PlayerPokedex playerPokedex) {
        PlayerPokedex updatedPlayerPokedex = playerPokedexRepository.saveAndFlush(playerPokedex);
        setCacheObject(generateCacheKey(PlayerPokedex.class.getName(), PlayerPokedex.GET_PLAYER_POKEDEX_CACHE_KEY, playerPokedex.getPlayer().getName()), updatedPlayerPokedex);
    }

    @Transactional(readOnly = true)
    public PlayerPokedex getPlayerPokedex(Player player) {
        String cacheKey = generateCacheKey(PlayerPokedex.class.getName(), PlayerPokedex.GET_PLAYER_POKEDEX_CACHE_KEY, player.getName());
        PlayerPokedex cacheData = (PlayerPokedex) getCacheObject(cacheKey, PlayerPokedex.class);
        if (cacheData != null) {
            return cacheData;
        } else {
            PlayerPokedex playerPokedex = playerPokedexRepository.findByPlayer(player);
            setCacheObject(cacheKey, playerPokedex);
            return playerPokedex;
        }
    }


}
